﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DaMain : MonoBehaviour {

    public Text t1, t2, t3, tek, rjeseno;
    int[] kod = new int[3] { 0, 0, 0 };
    List<Text> kodMjesta = new List<Text>();
    int p, d, t, rj;
    string txt;

    void Start()
    {
        rj = 0;
        rj = PlayerPrefs.GetInt("rjeseno");
        rjeseno.text = "Rjeseno: " + rj.ToString() + "/3";
        kodMjesta.Add(t1);
        kodMjesta.Add(t2);
        kodMjesta.Add(t3);
        p = UnityEngine.Random.Range(0, 9);
        d = UnityEngine.Random.Range(0, 9);
        t = UnityEngine.Random.Range(0, 9);
        print(p);
        print(d);
        print(t);
        txt = "Zbroj prve i zadnje znamenke je " + (p+t).ToString() + ".\n\nZbroj zadnje i druge znamenke je " + (d+t).ToString() + ".\n\nApsolutna razlika prve i zadnje znamenke je " + (Math.Abs(p-t)).ToString() + ".";
        tek.text = txt;
    }

    public void PromjenaPlus(int mjesto)
    {
        kod[mjesto] += 1;
        if (kod[mjesto] > 9) kod[mjesto] -= 10;
        kodMjesta[mjesto].text = kod[mjesto].ToString();
    }

    public void PromjenaMinus(int mjesto)
    {
        kod[mjesto] -= 1;
        if (kod[mjesto] < 0) kod[mjesto] += 10;
        kodMjesta[mjesto].text = kod[mjesto].ToString();
    }


    public void Provjeri()
    {
        if (kod[0]==p && kod[1]==d && kod[2] == t)
        {
            PlayerPrefs.SetInt("rjeseno", rj + 1);
            if (rj >= 2)
            {
                SceneManager.LoadScene(2);
            }
            else
            {
                SceneManager.LoadScene(1);
            }

        }
    }
}
