﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class OstaloScr : MonoBehaviour {

    public Text kod, kod2;
    string dobiveniKod = "88f12k3";
    string dobiveniKod2 = "1024";
    public GameObject kazaljka1, kazaljka2;
    float x,y;
    int brPokusaja=0;
    int lvl;

    private void Start()
    {
        lvl = PlayerPrefs.GetInt("rjeseno");
    }

    public void Dobij()
    {
        if (kod.text == dobiveniKod)
        {
            PlayerPrefs.SetInt("rjeseno", PlayerPrefs.GetInt("rjeseno")+1);
            SceneManager.LoadScene(3);
        }
    }

    public void Dobij2()
    {
        if (kod2.text == dobiveniKod2)
        {
            PlayerPrefs.SetInt("rjeseno", PlayerPrefs.GetInt("rjeseno") + 1);
            SceneManager.LoadScene(4);
        }
    }

    public void Osam()
    {
        PlayerPrefs.SetInt("rjeseno", PlayerPrefs.GetInt("rjeseno") + 1);
        SceneManager.LoadScene(5);
    }

    void Update()
    {
        if (lvl>5)
        {
            kazaljka1.transform.Rotate(0, 0, -Time.deltaTime * 24);
            kazaljka2.transform.Rotate(0, 0, -Time.deltaTime * 2);
            x = (360 - kazaljka1.transform.rotation.eulerAngles.z) / 6;
            y = (360 - kazaljka2.transform.rotation.eulerAngles.z) / 30;
        }
    }

    public void Sat()
    {
        if (Mathf.Abs(x - y) < 1.5f)
        {
            PlayerPrefs.SetInt("rjeseno", PlayerPrefs.GetInt("rjeseno") + 1);
            SceneManager.LoadScene(6);
        }
        else
        {
            brPokusaja += 1;
            if (brPokusaja > 5)
            {
                PlayerPrefs.SetInt("rjeseno", 0);
                SceneManager.LoadScene(0);
            }
        }
    }
}
