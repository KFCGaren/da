﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Pocetni : MonoBehaviour {

    int lvl;
	void Start () {
        lvl = PlayerPrefs.GetInt("rjeseno", 0);
	}
    public void Pocni()
    {
        if (lvl < 3)
        {
            SceneManager.LoadScene(1);
        }
        else
        {
            SceneManager.LoadScene(lvl - 1);
        }
    }
    public void Bonus()
    {
        PlayerPrefs.SetInt("rjeseno", 0);
        lvl = 0;
    }
}
